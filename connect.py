#! /usr/bin/env python

import json
import websockets
import requests
import time

from time import strftime
from math import inf
from log  import *


class ConnectionInitialiser(object):
    def __init__(self, token):
        """__init__()
        ConnInit init
        Initialises the ConnectionInitialiser
        
        Args:
        * token : User auth token
        """
        self.token = token


    async def connect(self):
        """connect()
        Connection function
        Creates connection to the Discord gateway
        
        Returns:
        * (socket, heartbeat_interval) : The socket object and heartbeat interval
        """
        
        LOG_INFO("Connecting")
    
        # Get WSS URL
        while True:
            try:
                wss_url = requests.get("https://discordapp.com/api/v6/gateway", headers={"Authorization" : "token"}).json()["url"]
                LOG_INFO(f"Websocket URL: {wss_url}")
                break
            except requests.exceptions.ConnectionError:
                LOG_WARN("Could not connect")
                for i in range(6):
                    time.sleep(1)
                    print("\r" + strftime("[%y%m%d-%H%M%S]") + "\033[93m [WARN]\033[0m: Retrying in " + str(5-i) + "s", end=" ")
                print()
        
        socket = await websockets.connect(wss_url, max_size=inf)
        LOG_INFO("Socket created")
        
        # Receive heartbeat interval
        heartbeat_interval = json.loads(await socket.recv())["d"]["heartbeat_interval"]
        LOG_INFO(f"heartbeat interval: {heartbeat_interval}ms")
        
        return (socket, heartbeat_interval)


    async def resume(self, socket, session_id, seq):
        """resume()
        Connection resume
        Resumes the connection to Discord
        
        Args:
        * socket     : WebSocket to use
        * session_id : Session id
        * seq        : Last sequence number
        """
        
        LOG_INFO("Resuming connection")
        LOG_INFO(f"Session ID : {session_id}")
        LOG_INFO(f"Sequence # : {seq}")
        await socket.send(json.dumps({"op" : 6, "d" : {"token" : self.token, "session_id" : session_id, "seq" : seq}}))
        LOG_INFO("Resume sent")
