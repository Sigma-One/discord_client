* Try to write readable code
* Remember adequate comments and/or docstrings
* Keep to the current docstring format
* Indent with 4 (four) spaces
* Separate anything more significant into it's own file where applicable (Example: `heartbeat.py`, `connect.py`)