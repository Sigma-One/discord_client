#! /usr/bin/env python

from log import *
import asyncio
import json
import websockets


class Heart(object):
    def __init__(self):
        """__init__()
        Heart init
        Initialises the heart
        """
        
        self.ack = True
    
    
    async def beat(self, socket, seq):
        """heartbeat()
        Heartbeating function
        Sends a heartbeat pulse if the previous one has been ACKed, otherwise closes socket
        
        Args:        
        * socket : WebSocket object to send heartbeat on
        * seq    : Sequence number
        
        Returns:
        * True  : if heartbeat successful
        * False : if ack hasn't been received
        """
        
        if self.ack:
            # Try to send payload
            try:
                await socket.send(json.dumps({"op" : 1, "d" : seq}))
            except websockets.exceptions.ConnectionClosedError:
                # Connection broke, sending failed
                LOG_WARN("Could not send heartbeat")
                return False
            
            LOG_INFO("Heartbeat sent")
            self.set_ack(False)
            return True
          
        else:
            # Connection possibly broke, no ACK
            LOG_WARN("No heartbeat ACK received")
            LOG_WARN("Connection terminating")
            return False
    
    
    def set_ack(self, state):
        """set_ack()
        Set heartbeat ack status
        
        Args:
        * state : Bool, True for successful ack
        """
        
        self.ack = state
