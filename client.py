#! /usr/bin/env python

import json               # Payload processing
import asyncio            # Async code (duh)
import websockets

from log import *
import time

from heartbeat import Heart
from identify  import identify
from connect   import ConnectionInitialiser
from secrets   import *


# Presence object template
# Please ignore
PRESENCE = {
  "status" : "online",                     # User status, "online", "away", or "dnd"
  "since"  : None,                         # Who knows
  "afk"    : False,                        # Something
  "game" : {
    "name"    : "Testing, Testing",        # Activity name, displayed under name
    "type"    : 0,                         # Activity type, 0 = "playing", 1 = "streaming", 2 = "listening to"
    "details" : "Python is fun",           # Line 1 of activity description
    "state"   : " ",                       # Line 2
  },
}



# Main class
class Main(object):
    def __init__(self):
        """__init__()
        Mostly a proxy function to just run __ainit__(), as __init__() itself cannot be asynchronous
        """
        LOG_INFO("Initializing")
        # Start asynchronous init
        asyncio.ensure_future(self.__ainit__())
        asyncio.get_event_loop().run_forever()
    
    
    async def __ainit__(self):
        """__ainit__()
        The real init function
        """
        
        self.conn_man = ConnectionInitialiser(TOKEN)
        
        await self.connect()
        
        self.heart = Heart()
        
        LOG_INFO("Init complete")
        
        LOG_INFO("Enabling loops")
        asyncio.ensure_future(self.heartbeat())
        LOG_INFO("Heartbeat : OK")
        asyncio.ensure_future(self.receiver() )
        LOG_INFO("Receiver  : OK")
    
    
    async def connect(self, resume=False):
        """connect()
        Connects to Discord and resumes if necessary
        
        Args:
        * resume : Bool indicating whether a previous connection should be resumed
        """
        
        # Init variables
        self.seq = None
        
        if resume:
            await self.socket.close(code=4040)
        
        connection_details = await self.conn_man.connect()
        
        self.socket             = connection_details[0]
        self.heartbeat_interval = connection_details[1]
        
        print(self.socket)
        
        if resume:
            await self.conn_man.resume(self.socket, self.session_id, self.seq)
            self.heart.set_ack(True)
        
        else:
            await self.identify()
    
    
    async def identify(self):
        """identify()
        Used to call the external identification function
        """
        
        await identify(self.socket, TOKEN)
        
        # Get session id from READY
        ready = json.loads(await self.socket.recv())
            
        self.session_id = ready["d"]["session_id"]
        self.guilds     = ready["d"]["guilds"]
            
        LOG_INFO(f"Session ID : {self.session_id}")
    
    
    async def output_event(self, event):
        #TODO
        """output_event()
        Will eventually output events on a local socket
        
        Args:
        * event : Event to output, probably in JSON form?
        """
        LOG_WARN("Socket stuff not implemented yet, printing event")
        print(event)
    
    
    async def receive_event(self):
        #TODO
        """receive_event()
        Will receive events from frontend over a local socket
        """
        LOG_WARN("Socket stuff not implemented yet")
    
    
    async def heartbeat(self):
        """heartbeat()
        Runs the main heartbeat loop
        Also reconnects in case of a disconnection
        """
        
        while True:
            code = await self.heart.beat(self.socket, self.seq)
            
            if not code:
                LOG_WARN("Heartbeat failure")
                await self.connect(resume=True)
                LOG_INFO("Reconnected")
                continue
            
            await asyncio.sleep(self.heartbeat_interval / 1000)
    
    
    async def receiver(self):
        """Main.receiver()
        Receiver function.
        Handles receiving payloads from the gateway.
        """
        
        while True:
            # Wait for data
            data = None
            try:
                data = json.loads(await self.socket.recv())
            except websockets.exceptions.ConnectionClosedError:
                LOG_WARN("Could not receive")
                await asyncio.sleep(3)
            
            if not data:
                continue
            
            # Increment sequence number
            self.seq = data["s"]
            
            # Re-identify on op 9
            if data["op"] == 9:
                LOG_WARN("Invalid session")
                time.sleep(3)
                LOG_INFO("Re-Identifying")
                await self.identify()
            
            # Set heartbeat ack on op 11
            if data["op"] == 11:
                LOG_INFO("Heartbeat ACK received")
                self.heart.set_ack(True)
            
            # Handle received messages
            #TODO
            if data["t"] == "MESSAGE_CREATE":
                LOG_INFO("Message received")
                pass


main = Main()
