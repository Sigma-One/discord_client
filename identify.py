#! /usr/bin/env python

from log import *

import asyncio
import json
import platform


async def identify(socket, token,
    useragent={"$os" : platform.system(), "$browser" : "python", "$device" : "computer"},
    presence={}):
    """identify()
    Identification function
    Sends the user identification payload
    
    Args:
    * socket    : WebSocket object to send heartbeat on
    * token     : Discord token to use for identification
    * useragent : Useragent dict to report to Discord
    * presence  : Rich presence dict to report to Discord
    
    Returns:
    * True
    """
    
    await socket.send(json.dumps(
        {
            "op" : 2,
            "d" : {"token" : token,
                "properties" : useragent,
                "presence": presence
            }
        }
    ))
    LOG_INFO("Identify payload sent")
    return True
